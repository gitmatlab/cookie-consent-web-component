# Cookie Consent Web Component

A web component for the footer, that allows you to set if cookies are allowed or no.


### Integration

Best approach is to start the server `npm start` and check the example integration: `src/index.html`

In the html head you need to include the script:
```html
<script src="cookie-consent.min.js" defer></script>
```
You'll find the minified script in the `build` folder.

Then use the `<cookie-consent>` tag.

```html
  <cookie-consent 
    cookieExpireDays="360"
    cookieName="cookie_consent"
    scripts="https://www.googletagmanager.com/gtag/js?id=xxxx, https://second.url.com/">
    <div class="about">
      Cookies help us with marketing. You can refuse them. More details can be 
      found here: <a href=\"\">Cookie-Info</a>.
    </div>
    <div class="buttons">
      <button data-action="accept">Accept all cookies</button>
      <button data-action="reject">Reject marketing cookdies</button>
    </div>
  </cookie-consent>
```

### Attributes

Two attributes are not required since they are set to these defaults:
 - `cookieExpireDays="360"`
 - `cookieName="cookie_consent"`

You probably want to include the `scripts`, that set the cookies. For example the Google Tag Manager:
 - `scripts="https://www.googletagmanager.com/gtag/js?id=xxxx)"`

The scripts attributes accepts a **coma separated list**:
 - `scripts="https://www.googletagmanager.com/gtag/js?id=xxxx), https://another.domain.com/"`

### How it works

When the user has pressed the `accept` or `reject` button, then the result will be stored in the browsers cookie store until it expired.

The cookie consent banner won't beshown, if the cookie is set.

When the cookie consent is `accept` then all scripts that uses cookies will load. Typical scripts that set cookies are marketing and tracking scripts. The Google Tag Manager for example. Usually the integration of these script are in the html header. You have to remove them there and put them as attribute in this component. See example above.

If the user clicks `reject` or doesn't click at all, then the scripts won't load and won't set cookies.

In case you have a script that wants to set an cookie, you could query document.cookie and check if the cookie consent is set to `accept` or `reject`. Check the source code for the `_hasCookieConsent()` function to get an idea how to do this.
