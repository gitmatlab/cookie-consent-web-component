import './cookie-consent.css'

class CookieConsent extends HTMLElement {
  constructor() {
    super()
    this.cookieExpireDays
    this.cookieName
    this.cookieConsent
    this.scripts
  }

  connectedCallback() {
    this.cookieExpireDays = this.getAttribute('cookie-expire-days') || 365
    this.cookieName = this.getAttribute('cookie-name') || 'cookie_consent'
    this.cookieConsent = this.getAttribute('cookie-consent')
    this.scripts = this.getAttribute('scripts')
      ? this.getAttribute('scripts').split(',').map(script => script.trim())
      : ''
    this._render()
  }

  _render() {
    if (this._hasCookieConsent()) {
      this.cookieConsent=this._getCookieValue()
      console.log("cookie consent is set to:", this.cookieConsent)
      this._hideCookieConsentBanner()
      this._loadScripts(this.scripts)
    } else {
      this._showCookieConsentBanner()
      this.addEventListener('click', (e) => this._clickHandler(e))
    }
  }

  _hasCookieConsent() {
    return document.cookie
      .split('; ')
      .filter(cookie => cookie.startsWith(`${this.cookieName}=`))
      .length > 0
  }

  _getCookieValue() {
    const value = document.cookie
      .split('; ')
      .find(cookie => cookie.startsWith(`${this.cookieName}=`))
    // value = 'cookie_consent=accept'
    return value.split('=')[1]
  }

  _setCookieConsent(response) {
    this.cookieConsent = response
    this._setCookie()
    this._render()
  }

  _setCookie() {
    const today = new Date()
    const expireDateString = new Date(
      today.getTime() + 60 * 60 * 24 * 1000 * this.cookieExpireDays)
      .toUTCString()
    document.cookie = `${this.cookieName}=${this.cookieConsent};expires=${expireDateString};path=/`
  }

  _showCookieConsentBanner() {
    this.classList.remove('remove-cookie-consent-banner')
    this.classList.remove('hide-cookie-consent-banner')
    this.classList.add('show-cookie-consent-banner')
  }

  _hideCookieConsentBanner() {
    // remove classes which for some reason are still there
    this.classList.remove('remove-cookie-consent-banner')
    this.classList.remove('show-cookie-consent-banner')
    // hide!
    this.classList.add('hide-cookie-consent-banner')
  }

  _clickHandler(event) {
    const { target } = event
    if (target?.dataset?.action === "accept") {
      this._setCookieConsent('accept')
    }
    if (target?.dataset?.action === "reject") {
      this._setCookieConsent('reject')
    }
  }

  _loadScripts(scripts) {
    console.log('load scripts...')
    scripts.map(script => {
      const scriptElement = document.createElement('script')
      scriptElement.src = script
      document.head.append(scriptElement)
    })
  }

}

customElements.define('cookie-consent', CookieConsent)
