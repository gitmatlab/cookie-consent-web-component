import copy from 'rollup-plugin-copy'
import { terser } from 'rollup-plugin-terser'
import postcss from 'rollup-plugin-postcss'
import cssnano from 'cssnano'
const bundleSize = require('rollup-plugin-bundle-size')

export default {
  input: './src/cookie-consent.js',
  output: {
    file: './dist/cookie-consent.min.js',
    format: 'es'
  },
  plugins: [
    terser(),
    bundleSize(),
    postcss({
      plugins: [cssnano]
    }),    
    copy({
      targets: [{ src: ['public/*', 'dist/*'], dest: 'build' }]
    }),
  ]
};
